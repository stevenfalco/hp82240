# This file is part of the HP82240 Emulator project.
# Copyright (C) 2006, 2009 Khanh-Dang Nguyen Thu-Lam <kdntl@yahoo.fr>
# Copyright (C) 2023 Steven A. Falco <stevenfalco@gmail.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

OBJDIR			:= build

LIBS			:=

DIRS			:= $(OBJDIR)

CFLAGS			+= -O2 -Wall -Werror -MD

LFLAGS			+=

SOURCES			:=						\
       			hp82240.c					\
		   	#

OBJECTS			:= $(SOURCES:%.c=$(OBJDIR)/%.o)

LIBLS			:= $(LIBS:%=-l%)
LIBDEPS			:= $(LIBS:%=$(LIBDIR)/lib%.a)

all: $(DIRS) $(OBJDIR)/hp82240

install: all
	$(announce)
	cp $(OBJDIR)/hp82240 ~/bin

dist: clean $(DIRS)
	cd .. && tar cfz /tmp/hp82240_emulator.tar.gz hp82240/
	mv -f /tmp/hp82240_emulator.tar.gz $(OBJDIR)
	@echo
	@echo You can now distribute hp82240_emulator.tar.gz
	@echo

$(OBJDIR)/hp82240: $(OBJECTS) $(LIBDEPS)
	$(announce)
	$(CC) $(CFLAGS) $(LFLAGS) -o $@ $(OBJECTS) $(LIBLS)

$(OBJDIR)/%.o:		%.c
	$(announce)
	$(CC) $(CFLAGS) -c -o $@ $(abspath $<)
	$(c_fix_depend)

$(DIRS):
	[ -d $(@) ] || mkdir -p $(@)

clean:
	$(announce)
	rm -fr $(OBJDIR)

# Include dependencies
include $(wildcard $(OBJDIR)/*.d)

announce = @echo; \
	   if [ -f "$@" ]; \
	   then echo "* $@: $?"; \
	   else echo "* $@:"; \
	   fi; \
	   echo

define c_fix_depend
	@-sed -e 's!^$(notdir $(<:%.c=%.o))!$(@)!' \
	  < $(OBJDIR)/$(notdir $(<:%.c=%.d)) \
	  > $(OBJDIR)/$(notdir $(<:%.c=%.d)).tmp
	@-mv -f $(OBJDIR)/$(notdir $(<:%.c=%.d)).tmp \
	        $(OBJDIR)/$(notdir $(<:%.c=%.d))
endef

define c_fix_depend_shared
	@-sed -e 's!^$(notdir $(<:%.c=%.o))!$(@)!' \
	  < $(OBJDIR)/shared/$(notdir $(<:%.c=%.d)) \
	  > $(OBJDIR)/shared/$(notdir $(<:%.c=%.d)).tmp
	@-mv -f $(OBJDIR)/shared/$(notdir $(<:%.c=%.d)).tmp \
	        $(OBJDIR)/shared/$(notdir $(<:%.c=%.d))
endef
