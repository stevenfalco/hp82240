# HP-82240 Emulator

This is an HP-82240 printer emulator, based on information given
at [hpcalc.org](http://www.hpcalc.org/details.php?id=4742)

It can read from a serial IR interface or from a file containing a capture of
printer data.

It produces a "Portable Bit Map"; i.e. a PBM file.  You can use the tools
at [NETPBM](https://netpbm.sourceforge.net/) to process this file format.

## Installation

Type `make` to compile the program.  If you want to install it into
your ~/bin directory, type `make install`.

## Usage

This program requires two command-line options, the file to read, and the
output file.  For example, if you have an IR interface on /dev/ttyS0, you
could do:

```
hp82240 /dev/ttyS0 output.pbm
```

In this example, you will need to interrupt the program by typing ^C after your
calculator has completed printing, because otherwise hp82240 won't know when
to stop.

Alternatively, you can tell hp82240 to exit after having processed a
specified number of lines.  Each line is 8 pixels high, so to capture 
a screenshot on a calculator with a 131x80 sized screen, you would want
to capture 80 / 8 = 10 lines:

```
hp82240 /dev/ttyS0 lcd.pbm 10
```

If you want to read from a file that you have already captured, simply do:

```
hp82240 some.captured.file output.pbm
```

In this case, hp82240 will exit after it finishes reading the input file.

## Converting the output

To convert the output into a postscript file, install the netpbm tools and
then do:

```
pnmtops -noturn output.pbm > output.ps
```

The output.ps file can then be sent to any postscript printer.

## Other

See hp82240.c for the changelog and TODO.
