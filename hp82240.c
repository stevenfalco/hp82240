// hp82240.c:
//
// This file is part of the HP82240 Emulator project.
// Copyright (C) 2006, 2009 Khanh-Dang Nguyen Thu-Lam <kdntl@yahoo.fr>
// Copyright (C) 2023 Steven A. Falco <stevenfalco@gmail.com
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// TODO:
// . Include the Roman8 font.
// . Output to another format than PBM.
// . Need testing
//
// Changelog:
// . 2023-12-19		- numerous fixups (SAF)
// . 2009-01-08:	- Add max lines feature
// . 2009-01-06:	- fix missing commas
//			- update the pbm's height at each end of line
// . 2006-02-08: first version

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include "hp82240.h"

#define HP82240_C_VERSION "0.4"

#define UNDERLINE(c)	((c) | (is_underlined ? (1 << (LINE_HEIGHT - 1)) : 0))

struct termios  tty;
struct termios  savetty;
int		inFile;

FILE		*pbmFile;
int		nb_lines;
unsigned int	nb_lines_max;

uint8_t		buffer[LINE_WIDTH];
int		current_column;

int		is_escaped;
int		is_expanded;
int		is_underlined;
int		is_ecma94;
int		data_remaining;

int		fd = -1;
int		need_terminal_reset = 0;

static void
write_height(FILE *f)
{
	rewind(f);
	fprintf(f, "P1\n%u %10u\n", LINE_WIDTH, nb_lines * LINE_HEIGHT);
	fseek(f, 0, SEEK_END);
}

static void
buffer_clear()
{
	int i;

	for(i = 0; i < LINE_WIDTH; ++i) {
		buffer[i] = 0;
	}
	current_column = 0;
}

static void
buffer_flush()
{
	int i;
	int j;

	for(i = 0; i < LINE_HEIGHT; i++) {
		for(j = 0; j < LINE_WIDTH; j++) {
			fprintf(pbmFile, "%c", (buffer[j] & (1 << i)) ? '1' : '0');
		}
		fprintf(pbmFile, "\n");
	}
	write_height(pbmFile);
	fflush(pbmFile);

	nb_lines++;
	buffer_clear();
}

static void
buffer_write(uint8_t c)
{
	int i;
	FONT *font = is_ecma94 ? &charmap_ecma94 : &charmap_roman8;

	if(current_column + (is_expanded ? 2 : 1) * CHAR_WIDTH > LINE_WIDTH) {
		buffer_flush();
	}

	for(i = 0; i < CHAR_WIDTH; ++i) {
		buffer[current_column] = UNDERLINE((*font)[c][i]);
		current_column++;

		if(is_expanded) {
			buffer[current_column] = buffer[current_column - 1];
			current_column++;
		}
	}
}

static void
printer_reset()
{
	is_ecma94	= 1;
	is_escaped	= 0;
	is_expanded	= 0;
	is_underlined	= 0;
	data_remaining	= 0;

	buffer_clear();
}

static void
printer_powerup()
{
	nb_lines = 0;
	printer_reset();
}

static void
printer_selftest()
{
	int i;

	printer_reset();

	for(i = 32; i <= 255; ++i) {
		buffer_write(i);
	}

	buffer_write(' ');
	buffer_write('D');
	buffer_flush();
	buffer_flush();

	for(i = 0; i < 6; ++i) {
		buffer_write("BAT: 5"[i]);
	}

	buffer_flush();
	buffer_flush();
	return;
}

static void
reset_device(int fd)
{
	if(need_terminal_reset) {
		if(tcsetattr(fd, TCSANOW, &savetty) != 0) {
			fprintf(stderr, "cannot restore attributes\n");
		}
	}
}

static int
setup_device(char *name)
{
	int i;
	int rc;

	if((fd = open(name, O_RDWR)) == -1) {
		fprintf(stderr, "cannot open %s\n", name);
		return -1;
	}

	if(isatty(fd)) {
		tcflush(fd, TCIFLUSH);

		rc = tcgetattr(fd, &savetty);
		if(rc < 0) {
			fprintf(stderr, "cannot get attributes %s\n", strerror(errno));
			return -1;
		}

		tty = savetty;

		tty.c_cflag = CLOCAL | CREAD | CS8;
		tty.c_iflag = IGNBRK;
		tty.c_oflag = 0;
		tty.c_lflag = 0;
		tty.c_line = 0;

		if(cfsetospeed(&tty, B115200) != 0) {
			fprintf(stderr, "cannot set output speed\n");
			return -1;
		}
		if(cfsetispeed(&tty, B0) != 0) {
			fprintf(stderr, "cannot set input speed\n");
			return -1;
		}

		for (i = 0; i < NCCS; i++) {
			tty.c_cc[i] = 0;
		}

		tty.c_cc[VMIN] = 1;
		tty.c_cc[VTIME] = 0;

		if(tcsetattr(fd, TCSANOW, &tty) != 0) {
			fprintf(stderr, "cannot set attributes\n");
			return -1;
		}
		need_terminal_reset = 1;
	}

	return fd;
}

static void
intHandler(int dummy)
{
	reset_device(fd);
	close(fd);

	write_height(pbmFile);
	fclose(pbmFile);

	exit(0);
}

int
main (int argc, char **argv)
{
	uint8_t c;
	int rc;

	if(argc < 3) {
		fprintf(stderr, "HP82240 Emulator, version %s\n", HP82240_C_VERSION);
		fprintf(stderr, "\n");
		fprintf(stderr, "Copyright (c) 2006, 2009 Khanh-Dang Nguyen Thu-Lam <kdntl@yahoo.fr>\n");
		fprintf(stderr, "Copyright (c) 2023 Steven A. Falco <stevenfalco@gmail.com\n");
		fprintf(stderr, "\n");
		fprintf(stderr, "This is free software; see the source for copying conditions.  There is NO\n");
		fprintf(stderr, "warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE,\n");
		fprintf(stderr, "to the extent permitted by law.\n");
		fprintf(stderr, "\n");
		fprintf(stderr, "Usage: %s <input> <output.pbm> [nmax]\n", argv[0]);

		exit(1);
	}

	signal(SIGINT, intHandler);

	nb_lines_max = (argc > 3) ? atoi(argv[3]) : -1;

	pbmFile = fopen(argv[2], "w+");
	if(!pbmFile) {
		perror(argv[2]);
		exit(1);
	}

	inFile = setup_device(argv[1]);
	if(inFile == -1) {
		perror(argv[1]);
		exit(1);
	}

	write_height(pbmFile);
	printer_powerup();

	while(nb_lines < nb_lines_max) { 
		if((rc = read(inFile, &c, 1)) != 1) {
			break;
		}

		if(data_remaining) {
			// Reading raw data.
			if(current_column < LINE_WIDTH) {
				buffer[current_column++] = c;
			}
			if(--data_remaining == 0) {
				buffer_flush();
			}
		} else if(is_escaped) {
			// Reading an escaped control code.
			switch(c) {
				case 255: printer_reset();
					  break;

				case 254: printer_selftest();
					  break;

				case 253:
					  is_expanded = 1;
					  buffer_flush();
					  break;

				case 252:
					  is_expanded = 0;
					  break;

				case 251:
					  is_underlined = 1;
					  buffer_flush();
					  break;

				case 250:
					  is_underlined = 0;
					  break;

				case 249:
					  is_ecma94 = 1;
					  buffer_flush();
					  break;

				case 248:
					  is_ecma94 = 0;
					  break;

				default:
					  data_remaining = c;
					  break;
			}
			is_escaped = 0; 
		} else {
			// Reading something else.
			switch(c) {
				case 0x1b:
					is_escaped = 1;
					break;

				case 0x04:
				case '\r':
					break;

				case '\t':
					buffer_write(' ');
					buffer_flush();
					break;

				case '\n':
					buffer_flush();
					break;

				default:
					buffer_write(c);
					break;
			}
		}
	}

	reset_device(inFile);
	close(inFile);

	// Write the final height of the output.
	write_height(pbmFile);
	fclose(pbmFile);

	exit(0);
}
